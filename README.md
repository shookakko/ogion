# Ogion

An nREPL server for Racket.

## A what now?

The [nREPL protocol](https://github.com/clojure/tools.nrepl/#why-nrepl)
allows developers to embed a server in their programs to which
external programs can connect for development, debugging, etc.

The original implementation of the protocol was written in Clojure,
and most clients assume they will connect to a Clojure server; however
the protocol is quite agnostic about what language is being
evaluated. It supports evaluating snippets of code or whole files with
input and output redirected back to the connected client.

Currently only tested with [monroe](https://github.com/sanel/monroe/)
as a client, which runs in Emacs. Other clients exist for Vim,
Eclipse, and Atom, as well as several independent command-line
clients; however these may require some adaptation to work with Racket.

## License

Copyright © 2017 Phil Hagelberg and contributors

Released under the terms of the GNU Lesser General Public License
version 3 or later; see the file LICENSE.
